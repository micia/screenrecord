#!/bin/sh

# This script is Public Domain.
#
# A basic screen recording script based on wf-recorder for sway,
# required executables:
# xdg-user-dir - to find out the user VIDEOS directory
# wf-recorder - for the actual screen recording
# jq - to parse JSON
# slurp - to select a screen area
# notify-send - for notification functionality
#
# Loosely based on:
# https://gist.github.com/ugursogukpinar/f390d9f4c829fb1b05fc74a12dd482bb
#
# Usage:
# screenrecord.sh - record whole screen
# screenrecord.sh -s - record screen area
# screenrecord.sh -w - record window
# screenrecord.sh -t - (may be used with any other option) if screenrecording
#                      is currently active, toggle it off and no recording takes
#                      place
#
# Author Lorenzo Cogotti, The DoubleFourteen Code Forge

pidfile="$XDG_RUNTIME_DIR/screenrecord.pid"
filename="$(xdg-user-dir VIDEOS)/ScreenCast_$(date +%F_%T).mp4"

isrunning() {
    pid=$(cat -- "$pidfile" 2>/dev/null)
    test $? && kill -s 0 -- "$pid" 2>/dev/null
}

usage() {
    printf -- "Usage: %s: [-s|-w] [-t]\n" "$(basename -- "$0" .sh)" >&2
    exit 1
}

# Parse arguments
while getopts swt opt; do
    case $opt in
    s) sflag=1 ;;
    w) wflag=1 ;;
    t) tflag=1 ;;
    ?) usage ;;
    esac
done

shift $(($OPTIND - 1))
[ $# -ne 0 ] && usage
[ ! -z "$sflag" ] && [ ! -z "$wflag" ] && usage

if ! isrunning; then
    # Start recording
    if [ ! -z "$sflag" ]; then
        wf-recorder -f "$filename" -g "$(slurp -c "#FFFFFF")" >/dev/null 2>&1 &
    elif [ ! -z "$wflag" ]; then
        wf-recorder -f "$filename" -g "$(swaymsg -t get_tree | \
            jq -r '.. | select(.pid? and .visible?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"' | \
            slurp -c "#FFFFFF" )" >/dev/null 2>&1 &
    else
        wf-recorder -f "$filename" >/dev/null 2>&1 &
    fi

    pid=$!
    printf -- "%d" "$pid" >"$pidfile" || exit 1

    trap 'rm -f "$pidfile"; trap - EXIT exit' EXIT INT HUP TERM
    wait -- "$pid"

    notify-send -e \
        "Screen recording complete" \
        "Video saved to: <i>$(basename -- "$filename" .mp4)</i>"

elif [ ! -z "$tflag" ]; then
    # Toggle recording off
    kill -s INT -- "$pid"
else
    printf -- "%s: operation refused, recording already in progress.\n" "$(basename -- "$0" .sh)" >&2
    exit 1
fi
