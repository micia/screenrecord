# Screen Record

`screenrecord.sh` is a mindless, yet useful,
script to record your screen in [sway](https://swaywm.org).

Really, it doesn't do anything else.

## Why?

Because I had enough bringing up shells, using tiresome aliases,
or, god forbid, firing up a whole OBS Studio just to record my screen.

I wanted a simple keyboard shortcut to toggle screen recording on and off at will,
and I have the impression other people may wish the same.

## Does it depend on anything?

Yes, to use `screenrecord.sh` you need:

* [xdg-user-dir](https://www.freedesktop.org/wiki/Software/xdg-user-dirs) to find out your user's VIDEOS directory.
* [wf-recorder](https://github.com/ammen99/wf-recorder) for the actual screen recording functionality.
* [mako](https://github.com/emersion/mako), or any other notification manager that may be triggered via `notify-send`.
* [jq](https://stedolan.github.io/jq) to parse JSON from command line.
* [slurp](https://wayland.emersion.fr/slurp) to select a screen area.

These lovely utilities are often already available on many `sway` setups.

## How do I use it?

1. Either clone the repo or copy the latest `screenrecord.sh`
   somewhere in your home. For the sake of this example, let's assume you
   placed `screenrecord.sh` in some `$HOME/bin` directory.

2. Customize your `~/.config/sway/config` and register some shortcuts
   to record your screen, e.g.:
   ```
     set $screenrecord "$HOME/bin/screenrecord.sh"
     bindsym $mod+r exec $screenrecord -t
     bindsym $mod+Shift+r exec $screenrecord -wt
     bindsym $mod+Control+r exec $screenrecord -st
   ```
   You should now be able to record your screen with those shortcuts.

3. If you're using a status bar (e.g. [i3status](https://github.com/i3/i3status)),
   you can easily customize it to react to `screenrecord.sh` pidfile,
   for example in your `~/.i3status.conf`:
   ```
     order += "run_watch screenrecord"

     run_watch screenrecord {
       pidfile = "/run/user/1000/screenrecord.pid"
       format = "🎥"
       format_down = ""
    }
   ```

## Special thanks

This script is loosely based on the following gist:

https://gist.github.com/ugursogukpinar/f390d9f4c829fb1b05fc74a12dd482bb

## License

Public domain, enjoy this code at will and use it at your own risk.

See [unlicense.org](https://unlicense.org/) for details or read the `LICENSE` file.

----------
Cheers :)
